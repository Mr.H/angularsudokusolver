import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { SudokuDisplayComponent } from './components/sudoku-display/sudoku-display.component';
import { SudokuInputComponent } from './components/sudoku-input/sudoku-input.component';

@NgModule({
  declarations: [
    AppComponent,
    SudokuInputComponent,
    SudokuDisplayComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
