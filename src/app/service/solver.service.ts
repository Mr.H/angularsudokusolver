import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { SolutionStep } from '../model/solution-step';
import { Cell } from './../model/cell';
import { Sudoku } from './../model/sudoku';

@Injectable({
  providedIn: 'root'
})
export class SolverService {

  constructor() { }

  solveCompletely(sudoku: Sudoku): SolutionStep[] {
    // if (sudoku.emptyCellCount > 51) {
    //   alert('It is not permitted to leave more than 51 fields blank.');
    //   return;
    // }
    const solution = this.initializeSolution(sudoku);
    let lastIndex = 0;
    while (solution[lastIndex].isNotFinal) {
      this.nextAction(solution);
      lastIndex = solution.length - 1;
    }
    return solution;
  }

  private initializeSolution(sudoku: Sudoku): SolutionStep[] {
    const stepAlternatives: SolutionStep[] = [];
    const previousState = new SolutionStep(sudoku);
    previousState.before = new Sudoku();
    previousState.likelyness = 1;
    stepAlternatives.push(previousState);
    return stepAlternatives;
  }

  private nextAction(stepAlternatives: SolutionStep[]) {
    const previousIndex = stepAlternatives.length - 1;
    const previousState = stepAlternatives[previousIndex];
    if (previousState.isNotFinal && previousState.progressHasBeenMade) {
      const nextSteps = this.getNextStep(previousState.after);
      if (!_.isEmpty(nextSteps)) {
        stepAlternatives.push(nextSteps);
      } else {
        this.backTrack(stepAlternatives);
      }
    }
    return stepAlternatives;
  }

  private backTrack(stepAlternatives: SolutionStep[]) {
    let lastIndex = stepAlternatives.length - 1;
    let done = false;
    while (!done && stepAlternatives.length > 0) {
      const currentStep = stepAlternatives[lastIndex];
      if (currentStep.remainingGuessingOptions.length >= 1) {
        currentStep.changedCell.value = currentStep.remainingGuessingOptions.pop();
        done = true;
      } else {
        stepAlternatives.pop();
        lastIndex = stepAlternatives.length - 1;
      }
    }
  }

  private getNextStep(before: Sudoku): SolutionStep {
    if (!this.isValid(before)) {
      return;
    }
    const emptyCells = before.emptyCells;
    let bestGuessingLikelyness = 0;
    let bestGuessingLikelynessCell: Cell;
    let guessingOptions: number[];
    emptyCells.forEach(beforeCell => {
      const usableValues = this.getUsableValues(before, beforeCell);
      if (usableValues.length > 0) {
        const likelyness = 1 / usableValues.length;
        if (likelyness > bestGuessingLikelyness) {
          bestGuessingLikelyness = likelyness;
          bestGuessingLikelynessCell = beforeCell;
          guessingOptions = usableValues;
        }
      }
    });
    const result = new SolutionStep(before);
    result.likelyness = bestGuessingLikelyness;
    result.remainingGuessingOptions = guessingOptions;
    const afterCell = result.getAfterCell(bestGuessingLikelynessCell);
    result.changedCell = afterCell;
    afterCell.value = guessingOptions.pop();
    return result;
  }

  private getUsableValues(sudoku: Sudoku, cell: Cell): number[] {
    const usedNumbersByRow = sudoku.usedNumbersByRow(cell.row).map;
    const usedNumbersByColumn = sudoku.usedNumbersByColumn(cell.column);
    const usedNumbersByQuadrant = sudoku.usedNumbersByQuadrant(cell.quadrant);
    const unusableValues = _.union(usedNumbersByRow, usedNumbersByColumn, usedNumbersByQuadrant);
    return _.difference([1, 2, 3, 4, 5, 6, 7, 8, 9], unusableValues);
  }

  isValid(sudoku: Sudoku): boolean {
    let index: number;
    let result = true;
    for (index = 1; index <= 9; index++) {
      result = result && this.containsEachNumberOnce(sudoku.usedNumbersByRow(index));
      if (!result) { break; }
      result = result && this.containsEachNumberOnce(sudoku.usedNumbersByColumn(index));
      if (!result) { break; }
      result = result && this.containsEachNumberOnce(sudoku.usedNumbersByQuadrant(index));
      if (!result) { break; }
    }
    return result;
  }

  private containsEachNumberOnce(numbers: number[]): boolean {
    const contains1Once = numbers.filter(number => number === 1).length <= 1;
    const contains2Once = numbers.filter(number => number === 2).length <= 1;
    const contains3Once = numbers.filter(number => number === 3).length <= 1;
    const contains4Once = numbers.filter(number => number === 4).length <= 1;
    const contains5Once = numbers.filter(number => number === 5).length <= 1;
    const contains6Once = numbers.filter(number => number === 6).length <= 1;
    const contains7Once = numbers.filter(number => number === 7).length <= 1;
    const contains8Once = numbers.filter(number => number === 8).length <= 1;
    const contains9Once = numbers.filter(number => number === 9).length <= 1;
    return contains1Once && contains2Once && contains3Once && contains4Once && contains5Once
      && contains6Once && contains7Once && contains8Once && contains9Once;
  }
}
