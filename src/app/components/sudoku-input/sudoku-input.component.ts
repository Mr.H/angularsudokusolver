import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Sudoku } from 'src/app/model/sudoku';

@Component({
  selector: 'app-sudoku-input',
  templateUrl: './sudoku-input.component.html',
  styleUrls: ['./sudoku-input.component.css']
})
export class SudokuInputComponent implements OnInit {

  constructor(private fb: FormBuilder) { }

  indices = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  form = this.fb.group({});

  get sudoku(): Sudoku {
    const sudoku = new Sudoku();
    this.indices.forEach(rowIndex => {
      this.indices.forEach(columnIndex => {
        sudoku.getCell(rowIndex, columnIndex).value = this.form.get(`${rowIndex}${columnIndex}`).value;
      });
    });
    sudoku.initialCells = sudoku.filledCells;
    return sudoku;
  }

  set sudoku(sudoku: Sudoku) {
    this.indices.forEach(rowIndex => {
      this.indices.forEach(columnIndex => {
        this.form.get(`${rowIndex}${columnIndex}`).setValue(sudoku.getCell(rowIndex, columnIndex).value);
      });
    });
  }

  ngOnInit() {
    this.indices.forEach(rowIndex => {
      this.indices.forEach(columnIndex => {
        this.form.addControl(`${rowIndex}${columnIndex}`, this.fb.control(''));
      });
    });
  }

  setValue(row: number, column: number, value: string) {
    const cell = this.sudoku.getCell(row, column);
    if (value) {
      cell.value = parseInt(value, 10);
    } else {
      delete cell.value;
    }
  }

  clear() {
    this.indices.forEach(rowIndex => {
      this.indices.forEach(columnIndex => {
        this.form.get(`${rowIndex}${columnIndex}`).setValue('');
      });
    });
  }
}
