import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SudokuDisplayComponent } from './sudoku-display.component';

describe('SudokuDisplayComponent', () => {
  let component: SudokuDisplayComponent;
  let fixture: ComponentFixture<SudokuDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SudokuDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SudokuDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
