import { Component, Input, OnInit } from '@angular/core';
import { Sudoku } from 'src/app/model/sudoku';

@Component({
  selector: 'app-sudoku-display',
  templateUrl: './sudoku-display.component.html',
  styleUrls: ['./sudoku-display.component.css']
})
export class SudokuDisplayComponent implements OnInit {

  @Input()
  sudoku: Sudoku;
  @Input()
  highlightedCell;
  @Input()
  likelyness: number;
  @Input()
  heading: string;

  constructor() { }

  ngOnInit() {
  }

}
