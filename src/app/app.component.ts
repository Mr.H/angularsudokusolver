import { Component, OnInit, ViewChild } from '@angular/core';
import { Sudoku } from 'src/app/model/sudoku';
import { SudokuInputComponent } from './components/sudoku-input/sudoku-input.component';
import { simpleExample } from './examples/simple-example';
import { complexExample } from './examples/complex-example';
import { SolutionStep } from './model/solution-step';
import { SolverService } from './service/solver.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'sudoku';
  solution: SolutionStep[];
  simpleExample: Sudoku;
  complexExample: Sudoku;

  @ViewChild(SudokuInputComponent)
  input: SudokuInputComponent;

  constructor(private service: SolverService) { }

  ngOnInit() {
    this.simpleExample = new Sudoku();
    simpleExample.data.forEach(cell => this.simpleExample.getCell(cell.row, cell.column).value = cell.value);
    this.complexExample = new Sudoku();
    complexExample.data.forEach(cell => this.complexExample.getCell(cell.row, cell.column).value = cell.value);
  }

  solve() {
    this.solution = this.service.solveCompletely(this.input.sudoku);
  }

  deleteSolution() {
    this.solution = [];
  }

  clearInput() {
    this.input.clear();
  }

  loadExample(example: Sudoku) {
    this.input.sudoku = example;
  }

  isInputValid() {
    const sudoku = this.input.sudoku;
    if (this.service.isValid(sudoku) && sudoku.isFinal) {
      alert('The input is valid');
    } else {
      alert('The input is not valid');
    }
  }

}
