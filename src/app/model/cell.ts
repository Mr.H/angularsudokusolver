
export class Cell {
  row: number;
  column: number;
  value: number;

  get isSet(): boolean {
    if (this.value) {
      return true;
    } else {
      return false;
    }
  }

  get isEmpty(): boolean {
    return !this.isSet;
  }

  get quadrant(): number {
    if (this.between(this.row, 1, 3) && this.between(this.column, 1, 3)) {
      return 1;
    } else if (this.between(this.row, 1, 3) && this.between(this.column, 4, 6)) {
      return 2;
    } else if (this.between(this.row, 1, 3) && this.between(this.column, 7, 9)) {
      return 3;
    } else if (this.between(this.row, 4, 6) && this.between(this.column, 1, 3)) {
      return 4;
    } else if (this.between(this.row, 4, 6) && this.between(this.column, 4, 6)) {
      return 5;
    } else if (this.between(this.row, 4, 6) && this.between(this.column, 7, 9)) {
      return 6;
    } else if (this.between(this.row, 7, 9) && this.between(this.column, 1, 3)) {
      return 7;
    } else if (this.between(this.row, 7, 9) && this.between(this.column, 4, 6)) {
      return 8;
    } else if (this.between(this.row, 7, 9) && this.between(this.column, 7, 9)) {
      return 9;
    } else {
      return null;
    }
  }

  static clone(cell: Cell): Cell {
    const clone = new Cell();
    clone.row = cell.row;
    clone.column = cell.column;
    clone.value = cell.value;
    return clone;
  }

  private between(input: number, lowerBounds: number, upperBounds: number) {
    return input >= lowerBounds && input <= upperBounds;
  }
}
