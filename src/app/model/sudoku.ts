import * as _ from 'lodash';
import { Cell } from './cell';
export class Sudoku {
  data: Cell[] = [];
  initialCells: Cell[];

  constructor() {
    let row: number;
    let column: number;
    let cell: Cell;
    for (row = 1; row <= 9; row++) {
      for (column = 1; column <= 9; column++) {
        cell = new Cell();
        cell.row = row;
        cell.column = column;
        this.data.push(cell);
      }
    }
  }

  get emptyCells(): Cell[] {
    return _.filter(this.data, (cell) => cell.isEmpty);
  }

  get filledCells(): Cell[] {
    return _.filter(this.data, (cell) => !cell.isEmpty);
  }

  get emptyCellCount(): number {
    return this.emptyCells.length;
  }

  get isFinal(): boolean {
    return this.emptyCellCount === 0;
  }

  get isNotFinal(): boolean {
    return !this.isFinal;
  }
  static clone(sudoku: Sudoku): Sudoku {
    const clone = new Sudoku();
    clone.data = sudoku.data.map(cell => Cell.clone(cell));
    clone.initialCells = clone.data.filter(cloneCell =>
      sudoku.initialCells.findIndex(initialCell =>
        initialCell.row === cloneCell.row && initialCell.column === cloneCell.column) >= 0);
    return clone;
  }

  isInitialCell(cell: Cell): boolean {
    return this.initialCells.findIndex(c => c.row === cell.row && c.column === cell.column) >= 0;
  }

  getCell(row: number, column: number): Cell {
    return _.find(this.data, cell => cell.row === row && cell.column === column);
  }

  filledCellsByRow(row: number): Cell[] {
    return _.filter(this.data, (cell => cell.row === row && cell.isSet));
  }

  filledCellsByColumn(column: number): Cell[] {
    return _.filter(this.data, (cell => cell.column === column && cell.isSet));
  }

  filledCellsByQuadrant(quadrant: number): Cell[] {
    return _.filter(this.data, (cell => cell.quadrant === quadrant && cell.isSet));
  }

  usedNumbersByRow(row: number): number[] {
    return this.filledCellsByRow(row).map(cell => cell.value);
  }

  usedNumbersByColumn(column: number): number[] {
    return this.filledCellsByColumn(column).map(cell => cell.value);
  }

  usedNumbersByQuadrant(quadrant: number): number[] {
    return this.filledCellsByQuadrant(quadrant).map(cell => cell.value);
  }

  toString(): string {
    let result = '';
    let rowIndex, columnIndex;
    for (rowIndex = 1; rowIndex <= 9; rowIndex++) {
      result += '|';
      for (columnIndex = 1; columnIndex <= 9; columnIndex++) {
        const value = this.getCell(rowIndex, columnIndex).value ? this.getCell(rowIndex, columnIndex).value.toString() : ' ';
        result += `${value}|`;
      }
      result += '\n';
    }
    return result;
  }
}
