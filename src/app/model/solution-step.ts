import { Cell } from './cell';
import { Sudoku } from './sudoku';

export class SolutionStep {
  before: Sudoku;
  after: Sudoku;
  changedCell: Cell;
  likelyness = 0;
  remainingGuessingOptions: number[];

  constructor(before: Sudoku) {
    this.before = before;
    this.after = Sudoku.clone(before);
  }

  get isFinal(): boolean {
    return this.after.isFinal;
  }

  get isNotFinal(): boolean {
    return this.after.isNotFinal;
  }

  get progressHasBeenMade(): boolean {
    return this.before.emptyCellCount > this.after.emptyCellCount;
  }

  getAfterCell(cell: Cell): Cell {
    return this.after.getCell(cell.row, cell.column);
  }
}
